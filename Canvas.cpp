#include "Canvas.h"

#include <TriMesh_algo.h>
#include <XForm.h>

#include <glm\gtx\string_cast.hpp>
using namespace trimesh;

Canvas::Canvas() { current_id = 0; }

trimesh::box transform_box(glm::mat4 trans, trimesh::box box) {
  glm::vec4 min, max;
  min = {box.min.x, box.min.y, box.min.z, 1};
  max = {box.max.x, box.max.y, box.max.z, 1};

  min = trans * min;
  max = trans * max;

  trimesh::point new_min{min.x, min.y, min.z};
  trimesh::point new_max{max.x, max.y, max.z};

  return trimesh::box(new_min, new_max);
}

void Canvas::redraw() {
  for (auto it1 = objects.begin(); it1 != objects.end(); ++it1) {
    for (auto it2 = objects.begin(); it2 != objects.end(); ++it2) {
      if (it1->second == it2->second) {
        continue;
      }

      trimesh::box bbox1 = it1->second->mesh->bbox;
      trimesh::box bbox2 = it2->second->mesh->bbox;

      trimesh::box new_b1 = transform_box(it1->second->transform, bbox1);
      trimesh::box new_b2 = transform_box(it2->second->transform, bbox2);

      if (new_b1.intersects(new_b2)) {
        it1->second->bounce(it2->second->get_velocity());
        it2->second->bounce(it2->second->get_velocity());
      }
    }
  }

  for (auto const &pair : objects) {
    pair.second->move(TIMESTEP);
    pair.second->render_vbo();
  }
}

int Canvas::add_object(GLuint shader_id, std::string filename) {
  objects[current_id] = std::make_unique<Object>(shader_id, filename);

  glCheckError();
  return current_id++;
}

void Canvas::remove_object(int id) {
  auto it = objects.find(id);
  objects.erase(it);
}

void Canvas::print() { std::cout << objects.size() << std::endl; }
void Canvas::print(int id) { objects[id]->print(); }

void Canvas::clear() {
  objects.clear();
  current_id = 0;
}

void Canvas::set_material(int id, glm::vec4 ambient, glm::vec4 diffuse,
                          glm::vec4 specular, float shininess) 
{
  objects[id]->set_material(ambient, diffuse, specular, shininess);
}

void Canvas::set_fixed(int id, bool fix) { objects[id]->set_fixed(fix); }
void Canvas::scale_and_center(int id) { objects[id]->scale_and_center();

};


void Canvas::transform(int id, glm::mat4 transform) {
  auto temp = objects[id]->transform;
  auto new_transform = transform * temp;
  objects[id]->transform = new_transform;
}

void Canvas::set_velocity(int id, glm::vec3 vel) {
  objects[id]->set_velocity(vel);
}