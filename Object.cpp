#include "Object.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm\gtx\string_cast.hpp>

#include "TriMesh_algo.h"
#include "util.h"

bool DEBUG = true;
const float TIMESTEP = 1.0f / 60.f;

Object::Object(GLuint shader, std::string filename) {
  mesh = trimesh::TriMesh::read(filename);
  shader_id = shader;
  scale_and_center();
  build_arrays();
  bind_vbo();
  mesh->need_bbox();
}

void computeNormal(float x1, float y1, float z1, float x2, float y2, float z2,
                   float x3, float y3, float z3, float* outputx, float* outputy,
                   float* outputz) {
  float v1x, v1y, v1z;
  float v2x, v2y, v2z;
  float cx, cy, cz;

  // find vector between x2 and x1
  v1x = x1 - x2;
  v1y = y1 - y2;
  v1z = z1 - z2;

  // find vector between x3 and x2
  v2x = x2 - x3;
  v2y = y2 - y3;
  v2z = z2 - z3;

  // cross product v1xv2

  cx = v1y * v2z - v1z * v2y;
  cy = v1z * v2x - v1x * v2z;
  cz = v1x * v2y - v1y * v2x;

  // normalize

  float length = sqrt(cx * cx + cy * cy + cz * cz);
  cx = cx / length;
  cy = cy / length;
  cz = cz / length;

  *outputx = cx;
  *outputy = cy;
  *outputz = cz;

  // glNormal3f(cx, cy, cz);
}

void Object::scale_and_center() {
  trimesh::point center = trimesh::mesh_center_of_mass(mesh);
  trimesh::trans(mesh, -center);
}

void Object::build_arrays() {
  glCheckError();
  vertex_vao = new GLfloat[mesh->vertices.size() * 3];
  if (vertex_vao == NULL) {
    std::cout << "Ran out of memory(vertex_vao)!" << std::endl;
    throw std::exception();
  }
  glCheckError();

  indicies_vao = new GLuint[mesh->faces.size() * 3];
  if (indicies_vao == NULL) {
    std::cout << "Ran out of memory(indicies_vao)!" << std::endl;
    throw std::exception();
  }
  glCheckError();

  normals_vao = new GLfloat[mesh->vertices.size() * 3];
  if (normals_vao == NULL) {
    std::cout << "Ran out of memory(normals_vao)!" << std::endl;
    return;
  }
  for (int i = 0; i < mesh->vertices.size() * 3; i++) {
    normals_vao[i] = 0.0;
  }
  glCheckError();

  int* numNormals = new int[mesh->vertices.size()];
  if (numNormals == NULL) {
    std::cout << "Ran out of memory(normal Counter)!" << std::endl;
    return;
  }
  for (int i = 0; i < mesh->vertices.size(); i++) {
    numNormals[i] = 0;
  }

  for (int i = 0; i < mesh->vertices.size(); i++) {
    vertex_vao[i * 3 + 0] = mesh->vertices[i].x;
    vertex_vao[i * 3 + 1] = mesh->vertices[i].y;
    vertex_vao[i * 3 + 2] = mesh->vertices[i].z;
  }

  unsigned k = 0;
  for (int i = 0; i < mesh->faces.size(); i++) {
    int index0 = mesh->faces[i][0];
    int index1 = mesh->faces[i][1];
    int index2 = mesh->faces[i][2];

    indicies_vao[k] = index0;
    indicies_vao[k + 1] = index1;
    indicies_vao[k + 2] = index2;

    k += 3;
  }

  for (int i = 0; i < mesh->faces.size() * 3; i = i + 3) {
    int index0 = indicies_vao[i];
    int index1 = indicies_vao[i + 1];
    int index2 = indicies_vao[i + 2];

    float outputx, outputy, outputz;
    // using the setNormal function from below we normalize the vectors
    computeNormal(vertex_vao[index0 * 3 + 0], vertex_vao[index0 * 3 + 1],
                  vertex_vao[index0 * 3 + 2], vertex_vao[index1 * 3 + 0],
                  vertex_vao[index1 * 3 + 1], vertex_vao[index1 * 3 + 2],
                  vertex_vao[index2 * 3 + 0], vertex_vao[index2 * 3 + 1],
                  vertex_vao[index2 * 3 + 2], &outputx, &outputy, &outputz);

    numNormals[index0]++;
    numNormals[index1]++;
    numNormals[index2]++;

    normals_vao[index0 * 3 + 0] += outputx;
    normals_vao[index0 * 3 + 1] += outputy;
    normals_vao[index0 * 3 + 2] += outputz;

    normals_vao[index1 * 3 + 0] += outputx;
    normals_vao[index1 * 3 + 1] += outputy;
    normals_vao[index1 * 3 + 2] += outputz;

    normals_vao[index2 * 3 + 0] += outputx;
    normals_vao[index2 * 3 + 1] += outputy;
    normals_vao[index2 * 3 + 2] += outputz;
  }
  for (int i = 0; i < mesh->vertices.size(); i++) {
    normals_vao[i * 3 + 0] = normals_vao[i * 3 + 0] / (float)(numNormals[i]);
    normals_vao[i * 3 + 1] = normals_vao[i * 3 + 1] / (float)(numNormals[i]);
    normals_vao[i * 3 + 2] = normals_vao[i * 3 + 2] / (float)(numNormals[i]);
  }

  delete[] numNormals;
  glCheckError();
}

void Object::bind_vbo() {
  glGenVertexArrays(1, &vao);
  glCheckError();
  glBindVertexArray(vao);
  glCheckError();
  glGenBuffers(1, &vertexVBO_id);
  glCheckError();
  // VERTEX VBO
  glBindBuffer(GL_ARRAY_BUFFER, vertexVBO_id);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * mesh->vertices.size() * 3,
               vertex_vao, GL_STATIC_DRAW);
  glCheckError();

  // INDEX VBO
  glGenBuffers(1, &indicesVBO_id);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO_id);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * mesh->faces.size() * 3,
               indicies_vao, GL_STATIC_DRAW);
  glCheckError();

  // POSITION ATTRIBUTE
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glCheckError();
  glEnableVertexAttribArray(0);
  glCheckError();

  // NORMAL VBO
  glGenBuffers(1, &normalVBO_id);
  glBindBuffer(GL_ARRAY_BUFFER, normalVBO_id);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * mesh->vertices.size() * 3,
               normals_vao, GL_STATIC_DRAW);
  glCheckError();

  // NORMAL ATTRIBUTE
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, 0);
  glEnableVertexAttribArray(1);
  glCheckError();
}

void Object::bounce(glm::vec3 normal) {
  if (fixed) {
    return;
  }
  velocity = -glm::reflect(velocity, normal);
  velocity += rand_tiny_vec();

  // velocity = -velocity;
  move(TIMESTEP * 2);
}

void Object::move(float timestep) {
  if (fixed) {
    return; 
  }
  transform = glm::translate(transform, timestep * velocity);
}

void Object::set_velocity(glm::vec3 vel) { velocity = vel; }

glm::vec3 Object::get_velocity() { return velocity; }

void Object::set_material(glm::vec4 ambient, glm::vec4 diffuse,
                          glm::vec4 specular, float shininess) 
{
  this->ambient = ambient;
  this->diffuse = diffuse;
  this->specular = specular;
  this->shininess = shininess;
}

void Object::render_vbo() {

  GLuint transformation = glGetUniformLocation(shader_id, "transform");
  glUniformMatrix4fv(transformation, 1, false, glm::value_ptr(transform));

  GLuint inverse_transpose = glGetUniformLocation(shader_id, "inverse_transpose_model");
  glUniformMatrix4fv(inverse_transpose, 1, false,
                     glm::value_ptr(glm::transpose(glm::inverse(transform))));

  GLuint ambientl
  = glGetUniformLocation(shader_id, "obj_mat.ambient");
  glUniform4fv(ambientl, 1, glm::value_ptr(ambient));

  GLuint diffusel = glGetUniformLocation(shader_id, "obj_mat.diffuse");
  glUniform4fv(diffusel, 1, glm::value_ptr(diffuse));

  GLuint specularl = glGetUniformLocation(shader_id, "obj_mat.specular");
  glUniform4fv(specularl, 1, glm::value_ptr(specular));

  GLuint shininessl = glGetUniformLocation(shader_id, "obj_mat.shininess");
  glUniform1f(shininessl, shininess);

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glBindVertexArray(vao);
  glUseProgram(shader_id);
  glDrawElements(GL_TRIANGLES, mesh->faces.size() * 3, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  glGetError();
}

void Object::print() {
  trimesh::point p = trimesh::point_center_of_mass(mesh->vertices);
  printf("%g %g %g\n", p[0], p[1], p[2]);
}