
// reference: https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Smooth_Specular_Highlights

in vec4 position;
in vec3 normal;
uniform mat4 m, v, p;

struct pointlight
{
  vec4 position;
  vec4 diffuse;
  vec4 specular;
};

pointlight light = pointlight(
  vec4(0.4,  0.4,  -0.4, 1.0),
  vec4(0.5,  0.5,  0.5, 1.0),
  vec4(0.5,  0.5,  0.5, 1.0)
);

vec4 scene_ambient = vec4(0.1, 0.1, 0.1, 1.0);
 
struct material
{
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  float shininess;
};

uniform material obj_mat;

void main()
{
  vec3 normal_direction = normalize(normal);
  vec3 view_direction = normalize(vec3(vec4(0.0, 0.0, 0.0, 1.0) - position));
  vec3 light_direction;
  
  vec3 line_of_sight = vec3(light.position - position);
  float distance = length(line_of_sight);
  light_direction = normalize(line_of_sight);
      
  vec3 ambient = vec3(scene_ambient) * vec3(obj_mat.ambient);
  
  vec3 diffuse = vec3(light.diffuse) * vec3(obj_mat.diffuse)
    * max(0.0, dot(normal_direction, light_direction));
  
  vec3 specular;
  if (dot(normal_direction, light_direction) < 0.0) {
      specular = vec3(0.0, 0.0, 0.0);
  } else {
      specular =vec3(light.specular) * vec3(obj_mat.specular) 
	* pow(max(0.0, dot(reflect(-light_direction, normal_direction), view_direction)), 
      obj_mat.shininess);
  }
  
  gl_FragColor = vec4(ambient + diffuse + specular, 1.0);
}
