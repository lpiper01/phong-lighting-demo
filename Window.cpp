#include "Window.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm\gtx\string_cast.hpp>
#include <sstream>

Window *Window::instance = nullptr;

Window::Window() {
  width = 100;
  height = 100;
  title = "Default";
  canvas = new Canvas;
  camera = new Camera;
  camera->reset();
  camera->orientLookAt({0.4f, 0.4f, -0.4f}, {0.0f, 0.0f, 0.0f},
                       {0.0f, 1.0f, 0.0f});
}

void Window::draw() {
  Window *window = get_window();

  window->snap_cam();
  glUniformMatrix4fv(4, 1, false,
                     glm::value_ptr(window->camera->getModelViewMatrix()));

  glUniformMatrix4fv(20, 1, false,
                     glm::value_ptr(window->camera->getProjectionMatrix()));
  




  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  window->canvas->redraw();
  glutSwapBuffers();
}

void Window::resize(int w, int h) {
  Window *window = get_window();
  glViewport(0, 0, w, h);
  window->camera->setScreenSize(w, h);
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glEnable(GL_DEPTH_TEST);
  glPolygonOffset(1, 1);
}

void Window::init(int width, int height, std::string title) {
  width = width;
  height = height;
  title = title;

  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
  glutInitContextVersion(3, 3);
  glutInitContextFlags(GLUT_CORE_PROFILE);
  glutInitWindowPosition(-1, -1);
  glutInitWindowSize(width, height);
  glutCreateWindow(title.c_str());
  glViewport(0, 0, width, height);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glPolygonOffset(1, 1);

  glutDisplayFunc(Window::draw);
  glutReshapeFunc(Window::resize);
  glutIdleFunc(Window::draw);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    std::cout << "Failed to init glew\n";
    throw std::exception("Failed to init glew");
  }

  std::cout << "glew initialized\n";
}

// http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/
void Window::init_shaders(std::string vert, std::string frag) {
  GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  std::string VertexShaderCode;
  std::ifstream VertexShaderStream(vert, std::ios::in);
  if (VertexShaderStream.is_open()) {
    std::stringstream sstr;
    sstr << VertexShaderStream.rdbuf();
    VertexShaderCode = sstr.str();
    VertexShaderStream.close();
  } else {
    std::cout << "Cannot open vertex shader\n";
    throw std::exception("Cannot open vertex shader");
  }

  std::string FragmentShaderCode;
  std::ifstream FragmentShaderStream(frag, std::ios::in);
  if (FragmentShaderStream.is_open()) {
    std::stringstream sstr;
    sstr << FragmentShaderStream.rdbuf();
    FragmentShaderCode = sstr.str();
    FragmentShaderStream.close();
  } else {
    std::cout << "Cannot open fragment shader\n";
    throw std::exception("Cannot open fragment shader");
  }

  GLint Result = GL_FALSE;
  int InfoLogLength;

  // Compile Vertex Shader
  printf("Compiling shader : %s\n", vert.c_str());
  char const *VertexSourcePointer = VertexShaderCode.c_str();
  glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
  glCompileShader(VertexShaderID);

  // Check Vertex Shader
  glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if (InfoLogLength > 0) {
    std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL,
                       &VertexShaderErrorMessage[0]);
    printf("%s\n", &VertexShaderErrorMessage[0]);
  }

  // Compile Fragment Shader
  printf("Compiling shader : %s\n", frag.c_str());
  char const *FragmentSourcePointer = FragmentShaderCode.c_str();
  glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
  glCompileShader(FragmentShaderID);

  // Check Fragment Shader
  glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if (InfoLogLength > 0) {
    std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL,
                       &FragmentShaderErrorMessage[0]);
    printf("%s\n", &FragmentShaderErrorMessage[0]);
  }

  // Link the program
  printf("Linking program\n");
  GLuint ProgramID = glCreateProgram();
  glAttachShader(ProgramID, VertexShaderID);
  glAttachShader(ProgramID, FragmentShaderID);
  glLinkProgram(ProgramID);

  // Check the program
  glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
  glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if (InfoLogLength > 0) {
    std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
    glGetProgramInfoLog(ProgramID, InfoLogLength, NULL,
                        &ProgramErrorMessage[0]);
    printf("%s\n", &ProgramErrorMessage[0]);
  }

  glDetachShader(ProgramID, VertexShaderID);
  glDetachShader(ProgramID, FragmentShaderID);

  glDeleteShader(VertexShaderID);
  glDeleteShader(FragmentShaderID);

  shader_id = ProgramID;
}

Window *Window::get_window() {
  if (instance == nullptr) {
    instance = new Window();
  }
  return instance;
}

Canvas *Window::get_canvas() { return canvas; }

Camera *Window::get_camera() { return camera; }

void Window::camera_bounds(trimesh::box bbox) {
  bounded = true;
  bounds = bbox;
}

void Window::snap_cam() {
  trimesh::point eyep(camera->eye.x, camera->eye.y, camera->eye.z);
  while (bounded && !bounds.contains(eyep)) {
    trimesh::vec3 center = bounds.center();
    glm::vec3 move = {center.x, center.y, center.z};
    move = move - camera->eye;
    move *= 0.001f;
    camera->eye += move;
    eyep = trimesh::point(camera->eye.x, camera->eye.y, camera->eye.z);
  }
}

void Window::run() { glutMainLoop(); }