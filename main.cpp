#include <glm/gtx/string_cast.hpp>
#include <sstream>
#include <array>
#include "Window.h"
#include "XForm.h"
#include "debug.h"
#include "util.h"

using namespace trimesh;

const float MOVEMENT_DELTA = 0.005f;
const float ANGLE_DELTA = 0.8f;
const std::string OBJECT_FILE = "data/bunny.ply";
const std::array<std::string, 3> OBJECT_FILE_LIST = {
    {"data/bunny.ply", "data/dragon.ply", "data/happy.ply"}};
const std::string PLANE_FILE = "data/plane.ply";

void build_room(Window* win, Canvas* canvas);
void init();

void key_callback(unsigned char key, int x, int y) {
  Window* win = Window::get_window();
  Camera* cam = win->get_camera();
  glm::vec3 look = cam->getLookVector();
  glm::vec3 up = cam->getUpVector();
  glm::vec3 left = cam->getLeftVector();
  glm::vec3 right = -left;
  switch (key) {
    case 'w': {
      cam->translate(look * MOVEMENT_DELTA);
      break;
    }
    case 'a': {
      cam->translate(left * MOVEMENT_DELTA);
      break;
    }
    case 's': {
      cam->translate(look * -MOVEMENT_DELTA);
      break;
    }
    case 'd': {
      cam->translate(right * MOVEMENT_DELTA);
      break;
    }
    case 'r': {
      win->get_canvas()->clear();
      build_room(win, win->get_canvas());
      init();
      break;
    }
    case 'f': {
      win->get_canvas()->clear();
      break;
    }
    case 'e': {
      cam->rotateW(ANGLE_DELTA);
      break;
    }
    case 'q': {
      cam->rotateW(-ANGLE_DELTA);
      break;
    }
    default: {
      break;
    }
  }
}

void special_callback(int key, int x, int y) {
  Window* win = Window::get_window();
  Camera* cam = win->get_camera();
  switch (key) {
    case GLUT_KEY_UP: {
      cam->rotateU(ANGLE_DELTA);
      break;
    }
    case GLUT_KEY_DOWN: {
      cam->rotateU(-ANGLE_DELTA);
      break;
    }
    case GLUT_KEY_LEFT: {
      cam->rotateV(ANGLE_DELTA);
      break;
    }
    case GLUT_KEY_RIGHT: {
      cam->rotateV(-ANGLE_DELTA);
      break;
    }
  }
}

void build_room(Window* win, Canvas* canvas) {
  glm::mat4 scale{1.0f};
  glm::mat4 rotate{1.0f};
  glm::mat4 trans{1.0f};
  rotate = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), {1, 0, 0});
  scale = glm::scale(glm::mat4(1.0f), {0.5f, 0.5f, 0.5f});
  trans = glm::translate(glm::mat4(1.0f), {0, -0.5, 0});

  // FLOOR AND CEILING
  int obj = canvas->add_object(win->shader_id, PLANE_FILE);
  canvas->transform(obj, rotate);
  canvas->transform(obj, scale);
  canvas->transform(obj, trans);
  canvas->set_fixed(obj, true);

  obj = canvas->add_object(win->shader_id, PLANE_FILE);
  canvas->transform(obj, rotate);
  canvas->transform(obj, scale);
  canvas->transform(obj, glm::inverse(trans));
  canvas->set_fixed(obj, true);

  // FRONT AND BACK WALLS
  rotate = glm::rotate(glm::mat4(1.0f), glm::radians(0.0f), {1, 0, 0});
  scale = glm::scale(glm::mat4(1.0f), {0.5f, 0.5f, 0.5f});
  trans = glm::translate(glm::mat4(1.0f), {0.0f, 0.0f, 0.5f});

  obj = canvas->add_object(win->shader_id, PLANE_FILE);
  canvas->transform(obj, rotate);
  canvas->transform(obj, scale);
  canvas->transform(obj, trans);
  canvas->set_fixed(obj, true);

  obj = canvas->add_object(win->shader_id, PLANE_FILE);
  canvas->transform(obj, rotate);
  canvas->transform(obj, scale);
  canvas->transform(obj, glm::inverse(trans));
  canvas->set_fixed(obj, true);

  // LEFT AND RIGHT WALLS
  rotate = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), {0, 1, 0});
  scale = glm::scale(glm::mat4(1.0f), {0.5f, 0.5f, 0.5f});
  trans = glm::translate(glm::mat4(1.0f), {0.5f, 0.0f, 0.0f});

  obj = canvas->add_object(win->shader_id, PLANE_FILE);
  canvas->transform(obj, rotate);
  canvas->transform(obj, scale);
  canvas->transform(obj, trans);
  canvas->set_fixed(obj, true);

  obj = canvas->add_object(win->shader_id, PLANE_FILE);
  canvas->transform(obj, rotate);
  canvas->transform(obj, scale);
  canvas->transform(obj, glm::inverse(trans));
  canvas->set_fixed(obj, true);
}

std::string random_file() 
{ 
    int num = rand() % OBJECT_FILE_LIST.size() + 0; 
    return OBJECT_FILE_LIST[num];
}

void random_obj(Window* win) {
  Canvas* canvas = win->get_canvas();

  float rand_scale = abs(rand_small());
  glm::vec3 scale_vec{rand_scale, rand_scale, rand_scale};
  int obj = canvas->add_object(win->shader_id, random_file());
  
  glm::mat4 transform(1.0f);
  transform = glm::scale(transform, scale_vec);
  transform = glm::translate(transform, rand_small_vec());
  canvas->transform(obj, transform);
  canvas->set_velocity(obj, rand_small_vec());
  
  glm::vec4 ambient = {rand_color(), 1};
  glm::vec4 diffuse = {rand_color(), 1};
  glm::vec4 specular = {rand_color(), 1};
  canvas->set_material(obj, ambient, diffuse, specular, 5.0f);
}

void init() {
  Window* win = Window::get_window();
  Canvas* canvas = win->get_canvas();
 
  build_room(win, canvas);
  
  for (int i = 0; i < 15; i++) {
    random_obj(win);
  }

  trimesh::point3 max, min;
  min = {-0.45, -0.45, -0.45};
  max = {0.45, 0.45, 0.45};
  win->camera_bounds(trimesh::box(min, max));
}

int main(int argc, char** argv) {
  trimesh::TriMesh::set_verbose(0);
  Window* win = Window::get_window();
  Canvas* canvas = win->get_canvas();
  glutInit(&argc, argv);
  win->init(600, 300, "Not Default");
  glCheckError();
  win->init_shaders("test.vert", "test.frag");
  glCheckError();

  init();

  glutKeyboardFunc(key_callback);
  glutSpecialFunc(special_callback);
  glCheckError();
  win->run();
  return 1;
}