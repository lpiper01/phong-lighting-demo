#pragma once
#include <TriMesh.h>

#include <glm/glm.hpp>

#include "debug.h"

extern const float TIMESTEP;

class Object {
 public:
  Object(GLuint shader, std::string filename);
  trimesh::TriMesh* mesh;
  GLuint vertexVBO_id, indicesVBO_id, normalVBO_id;
  GLfloat* vertex_vao;
  GLuint* indicies_vao;
  GLfloat* normals_vao;
  GLuint vao;
  GLuint shader_id;

  glm::mat4 transform = glm::mat4(1.0f);
  void render_vbo();
  void print();
  void bounce(glm::vec3 normal);
  void move(float timestep);
  void set_velocity(glm::vec3 vel);
  void set_fixed(bool fix) { fixed = fix; };
  glm::vec3 get_velocity();
  void set_material(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular,
                    float shininess);
  void scale_and_center();
 private:

  void build_arrays();
  void bind_vbo();

  glm::vec4 ambient = {0.2, 0.2, 0.2, 1.0};
  glm::vec4 diffuse = {1.0, 0.8, 0.8, 1.0};
  glm::vec4 specular = {1.0, 1.0, 1.0, 1.0};
  float shininess = 5;

  bool fixed = false;
  glm::vec3 velocity;
};
