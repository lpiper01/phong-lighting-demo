#include "Camera.h"
Camera::Camera() { reset(); }

Camera::~Camera() {}

void Camera::reset() {
  orientLookAt(glm::vec3(0.0f, 0.0f, DEFAULT_FOCUS_LENGTH),
               glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
  setViewAngle(VIEW_ANGLE);
  setNearPlane(NEAR_PLANE);
  setFarPlane(FAR_PLANE);
  screenWidth = screenHeight = 200;
  screenWidthRatio = 1.0f;
  rotU = rotV = rotW = 0.f;
  w = glm::normalize(-1.f * look);
  u = glm::normalize(glm::cross(up, w));
  v = glm::cross(w, u);
}

// called by main.cpp as a part of the slider callback for controlling rotation
// the reason for computing the diff is to make sure that we are only
// incrementally rotating the camera
void Camera::setRotUVW(float _u, float _v, float _w) {
  float diffU = _u - rotU;
  float diffV = _v - rotV;
  float diffW = _w - rotW;
  rotateU(diffU);
  rotateV(diffV);
  rotateW(diffW);
  rotU = _u;
  rotV = _v;
  rotW = _w;
}

void Camera::orientLookAt(glm::vec3 eyePoint, glm::vec3 lookatPoint,
                          glm::vec3 upVec) {
  eye = eyePoint;
  look = glm::normalize(lookatPoint - eyePoint);
  up = glm::normalize(upVec);
  w = glm::normalize(-1.f * look);
  u = glm::normalize(glm::cross(up, w));
  v = glm::cross(w, u);
}

void Camera::orientLookVec(glm::vec3 eyePoint, glm::vec3 lookVec,
                           glm::vec3 upVec) {
  eye = eyePoint;
  look = glm::normalize(lookVec);
  up = glm::normalize(upVec);
  w = glm::normalize(-1.f * look);
  u = glm::normalize(glm::cross(up, w));
  v = glm::cross(w, u);
}

glm::mat4 Camera::getScaleMatrix() {
  glm::mat4 scaleMat(1.0f);
  float x, y, z;
  float viewAngle_radians = viewAngle * (PI / 180.f);
  x = 1.f / (tan(viewAngle_radians / 2.f) * farPlane);
  y = 1.f /
      (tan(viewAngle_radians / 2.f) * farPlane * screenHeight / screenWidth);
  z = 1.f / farPlane;
  scaleMat[0][0] = x;
  scaleMat[1][1] = y;
  scaleMat[2][2] = z;
  scaleMat[3][3] = 1;
  return scaleMat;
}

glm::mat4 Camera::getInverseScaleMatrix() {
  // TODO: Do this more efficiently
  return glm::inverse(getScaleMatrix());
}

glm::mat4 Camera::getUnhingeMatrix() {
  glm::mat4 unhingeMat4(1.0);
  float c = -1.f * nearPlane / farPlane;
  unhingeMat4[0][0] = 1.f;
  unhingeMat4[1][1] = 1.f;
  unhingeMat4[2][2] = -1.f / (c + 1.f);
  unhingeMat4[3][2] = c / (c + 1.f);
  unhingeMat4[2][3] = -1.f;
  unhingeMat4[3][3] = 0.f;
  return unhingeMat4;
}

glm::mat4 Camera::getProjectionMatrix() {
  glm::mat4 projMat4(1.0);
  projMat4 = getUnhingeMatrix() * getScaleMatrix();
  return projMat4;
}

glm::mat4 Camera::getInverseModelViewMatrix() {
  // TODO: do this more efficiently
  return glm::inverse(getModelViewMatrix());
}

void Camera::setViewAngle(float _viewAngle) { viewAngle = _viewAngle; }

void Camera::setNearPlane(float _nearPlane) { nearPlane = _nearPlane; }

void Camera::setFarPlane(float _farPlane) { farPlane = _farPlane; }

void Camera::setScreenSize(int _screenWidth, int _screenHeight) {
  screenWidth = _screenWidth;
  screenHeight = _screenHeight;
  screenWidthRatio = (float)screenWidth / (float)screenHeight;
}

glm::mat4 Camera::getModelViewMatrix() {
  glm::mat4 translation(1.0f);
  glm::mat4 rotation(1.0f);

  rotation[0] = glm::vec4(u.x, v.x, w.x, 0.f);
  rotation[1] = glm::vec4(u.y, v.y, w.y, 0.f);
  rotation[2] = glm::vec4(u.z, v.z, w.z, 0.f);
  rotation[3] = glm::vec4(0.f, 0.f, 0.f, 1.f);

  translation[3] = glm::vec4(-1.f * eye, 1.f);
  return rotation * translation;
}

// multiplying vector by matrix method from
// https://stackoverflow.com/a/20923902
void Camera::rotateV(float degrees) {
  glm::mat4 rot = glm::rotate(glm::mat4(1.0f), glm::radians(degrees), v);
  u = glm::vec3(rot * glm::vec4(u, 0.0f));
  w = glm::vec3(rot * glm::vec4(w, 0.0f));
  look = -w;
}

void Camera::rotateU(float degrees) {
  glm::mat4 rot = glm::rotate(glm::mat4(1.0f), glm::radians(1.0f * degrees), u);
  v = glm::vec3(rot * glm::vec4(v, 0.0f));
  w = glm::vec3(rot * glm::vec4(w, 0.0f));
  look = -w;
}

void Camera::rotateW(float degrees) {
  glm::mat4 rot =
      glm::rotate(glm::mat4(1.0f), glm::radians(-1.0f * degrees), w);
  u = glm::vec3(rot * glm::vec4(u, 0.0f));
  v = glm::vec3(rot * glm::vec4(v, 0.0f));
  look = -w;
}

void Camera::translate(glm::vec3 v) { eye += v; }

void Camera::rotate(glm::vec3 point, glm::vec3 axis, float degrees) {}

glm::vec3 Camera::getEyePoint() { return eye; }

glm::vec3 Camera::getLookVector() { return look; }

glm::vec3 Camera::getUpVector() { return up; }

float Camera::getViewAngle() { return viewAngle; }

float Camera::getNearPlane() { return nearPlane; }

float Camera::getFarPlane() { return farPlane; }

int Camera::getScreenWidth() { return screenWidth; }

int Camera::getScreenHeight() { return screenHeight; }

float Camera::getScreenWidthRatio() { return screenWidthRatio; }

glm::vec3 Camera::getLeftVector() { return -u; };