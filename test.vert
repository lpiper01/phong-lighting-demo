// reference: https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Smooth_Specular_Highlights

#version 330 core
#extension GL_ARB_explicit_uniform_location : enable

layout (location = 0) in vec3 v_coord;
layout (location = 1) in vec3 v_normal;

layout (location = 4) uniform mat4 modelview;
layout (location = 20) uniform mat4 projection;
layout (location = 36) uniform mat4 transform;
layout (location = 52) uniform mat4 inverse_transpose_model;

out vec4 position;
out vec3 normal;

void main()
{
  position = transform * vec4(v_coord, 1.0);
  normal = vec3(normalize(inverse_transpose_model * vec4(v_normal, 0.0)));

  mat4 mvp = projection*modelview*transform;
  gl_Position = mvp * vec4(v_coord, 1.0);
}