#pragma once
#include <TriMesh.h>
#include <TriMesh_algo.h>

#include <glm/ext/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <unordered_map>
#include <vector>

#include "Object.h"
#include "debug.h"

using objptr = std::unique_ptr<Object>;

class Canvas {
 public:
  Canvas();

  // Renders objects to screen
  void redraw();

  // Adds an object with a given shader program and filename
  int add_object(GLuint shader_id, std::string filename);

  // Removes object pointed to by id
  void remove_object(int id);

  // Debug data
  void print();
  void print(int id);

  // Removes all objects from Canvas
  void clear();

  // Set material properties
  void set_material(int id, glm::vec4 ambient, glm::vec4 diffuse,
                    glm::vec4 specular, float shininess);

  // Transforms given object
  void transform(int id, glm::mat4 transform);

  void set_velocity(int id, glm::vec3 vel);

  // Sets whether or not an object is able to move
  void set_fixed(int id, bool fix);

  void scale_and_center(int id);
 
 private:
  // Point lightsource location
  glm::vec3 point_light;

  // Next ID to be assigned to anobject
  int current_id;

  // Map of IDs to objects
  std::unordered_map<int, objptr> objects;
};
