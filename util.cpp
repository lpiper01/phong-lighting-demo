#include "util.h"

const float TINY_MIN = -0.005f;
const float TINY_MAX = 0.005f;

const float SMALL_MIN = -0.3f;
const float SMALL_MAX = 0.3f;

const float MED_MIN = 0.0f;
const float MED_MAX = 1.0f;

float RandomFloat(float a, float b) {
  float random = ((float)rand()) / (float)RAND_MAX;
  float diff = b - a;
  float r = random * diff;
  return a + r;
}

float rand_tiny() { return RandomFloat(TINY_MIN, TINY_MAX); }

float rand_small() { return RandomFloat(SMALL_MIN, SMALL_MAX); }

float rand_medium() { return RandomFloat(MED_MIN, MED_MAX); }

glm::vec3 rand_tiny_vec() { return {rand_tiny(), rand_tiny(), rand_tiny()}; }

glm::vec3 rand_small_vec() {
  return {rand_small(), rand_small(), rand_small()};
}

glm::vec3 rand_med_vec() {
  return {rand_medium(), rand_medium(), rand_medium()};
}

glm::vec3 rand_color() { return rand_med_vec(); }