#pragma once
#include <GL/glew.h>
#include <GL/freeglut.h>


#include <iostream>

GLenum glCheckError_(const char *file, int line);

#define glCheckError() glCheckError_(__FILE__, __LINE__)
