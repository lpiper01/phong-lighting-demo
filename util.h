#include <glm/glm.hpp>

#pragma once
extern const float TINY_MIN;
extern const float TINY_MAX;

extern const float SMALL_MIN;
extern const float SMALL_MAX;

extern const float MED_MIN;
extern const float MED_MAX;


float RandomFloat(float a, float b);
float rand_tiny();
float rand_small();
float rand_medium();

glm::vec3 rand_tiny_vec();
glm::vec3 rand_small_vec();
glm::vec3 rand_med_vec();
glm::vec3 rand_color();
