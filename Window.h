#include <string>

#include "Box.h"
#include "Camera.h"
#include "Canvas.h"
#include "debug.h"

#pragma once
class Window {
 public:
  void init(int width, int height, std::string title);
  void init_shaders(std::string vert, std::string frag);
  static Window *get_window();
  Canvas *get_canvas();
  Camera *get_camera();

  void camera_bounds(trimesh::box bbox);
  void run();
  GLuint shader_id;

 private:
  Window();
  static void draw();
  static void resize(int w, int h);
  void snap_cam();

  Canvas *canvas;
  Camera *camera;
  bool bounded = false;
  trimesh::box bounds;

  int width, height;
  std::string title;

  static Window *instance;
};
